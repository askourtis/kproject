import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.32"
}

repositories {
    mavenCentral()
}


subprojects {

    apply(plugin="kotlin")

    group   = "com.sheach"
    version = "1.0-SNAPSHOT"

    repositories {
        mavenCentral()
        maven { url = uri("https://papermc.io/repo/repository/maven-public/") }
    }

    dependencies {
        implementation(kotlin("stdlib"))
        compileOnly("io.papermc.paper:paper-api:1.18.1-R0.1-SNAPSHOT")
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    java {
        toolchain.languageVersion.set(JavaLanguageVersion.of(17))
    }


    tasks.processResources {
        filesMatching("plugin.yml") {
            expand(
                "name"    to this@subprojects.name,
                "main"    to "${this@subprojects.group}.${this@subprojects.name}",
                "version" to this@subprojects.version,
                "api"     to "1.18",
                "author"  to "sheach",
                "core"    to project(":KCore").name
            )
        }
    }
}


project(":KCore") {
    tasks.jar {
        from(configurations.compileOnly.map { it.asFileTree })
    }
}